package ru.csu.springsecurity.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.csu.springsecurity.dao.entity.User;
import ru.csu.springsecurity.dto.request.RegRequest;
import ru.csu.springsecurity.dto.response.JwtResponse;
import ru.csu.springsecurity.service.impl.SecurityService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequiredArgsConstructor
public class RegContoller {
    private final SecurityService securityService;

    @PostMapping("/registration")
    @ResponseBody
    public int regIn(@RequestBody RegRequest request){
        return securityService.register(request);
    }
}
