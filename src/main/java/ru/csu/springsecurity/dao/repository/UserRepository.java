package ru.csu.springsecurity.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ru.csu.springsecurity.dao.entity.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
