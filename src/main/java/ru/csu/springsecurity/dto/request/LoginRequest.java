package ru.csu.springsecurity.dto.request;

import lombok.Value;

@Value
public class LoginRequest {
    String username;
    String password;
}
